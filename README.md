# atlShell

Simple wrapper for promisify-child-process.  Takes either a String or an Array.

See https://github.com/jcoreio/promisify-child-process

See also atlSudo for sudo usage:
https://bitbucket.org/ATLTed/atl-sudo/src/master/


## Install

```shell
npm install git+https://bitbucket.org/ATLTed/atl-shell.git#master
```

## Example Usage

```javascript
const atlShell = require('atl-shell');

run();

async function run() {
  // Simple usage with String
  await atlShell('mkdir -p /tmp/atlShell');

  await atlShell('open /tmp/atlShell');

  // String or Array usage wish stdout or stederr:
  
  // Use closures since we're defining the const stdout more
  // that once (and it can't be named to something else);
  // It returns an object like {stdout:'', stderr: ''}
  {
    const { stdout, stderr } = await atlShell('ls -l /tmp');
    console.log(stdout);
  }

  {
    const { stdout, stderr } = await atlShell([ 'ls', '-l', '/tmp' ]);
    console.log(stdout);
  }
  
}
```