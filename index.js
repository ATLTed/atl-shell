"use strict";

exports = module.exports = shell;

const { spawn } = require('promisify-child-process');

function shell(command, options={}) {
  const setDefaultOptions = (k,v) => options[k] = options[k] === undefined ? v :  options[k];
  setDefaultOptions('encoding', 'utf8');
  command = typeof command === "string" ? command.split(' ') : command;

  let _command = command.shift();
  let arg = command;
  
  return spawn(_command, arg, options);
}