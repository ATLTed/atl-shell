const atlShell = require('./index.js');

run();

async function run() {
  // Simple usage with String
  await atlShell('mkdir -p /tmp/atlShell');

  await atlShell('open /tmp/atlShell');

  // String or Array usage wish stdout or stederr:
  
  // Use closures since we're defining the const stdout more
  // that once (and it can't be named to something else);
  // It returns an object like {stdout:'', stderr: ''}
  {
    const { stdout, stderr } = await atlShell('ls -l /tmp');
    console.log(stdout);
  }

  {
    const { stdout, stderr } = await atlShell([ 'ls', '-l', '/tmp' ]);
    console.log(stdout);
  }
  
}